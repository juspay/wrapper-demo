package in.juspay.ectest;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.security.ProviderInstaller;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import in.juspay.godel.core.PaymentConstants;

public class API {

    private static final String LOG_TAG = "APIHelper";

    private static final String API_KEY = "4BBA5347E65542C5A56EF1B8C184DD43";

    static void createCustomer(String merchantId, String customerId, String mobile, String email, Context context) {
        try {
            String url = context.getString(R.string.base_url) + "/customers";
            String auth = Base64.encodeToString((API_KEY + ":").getBytes(), Base64.DEFAULT);
            JuspayHTTPResponse response = null;

            HttpsURLConnection connection = (HttpsURLConnection) (new URL(url).openConnection());
            connection.setRequestMethod("POST");
            connection.setSSLSocketFactory(new TLSSocketFactory());
            connection.setRequestProperty("Authorization", "Basic " + auth);
            connection.setRequestProperty("version", "2018-07-01");
            connection.setDoOutput(true);

            Map<String, String> payload = new HashMap<>();
            payload.put("object_reference_id", customerId);
            payload.put("mobile_number", mobile);
            payload.put("email_address", email);
            payload.put("first_name", "Harsh");
            payload.put("last_name", "Garg");
            payload.put(PaymentConstants.DESCRIPTION, "Test Transaction");
            payload.put("options.get_client_auth_token", "true");

            initializeSSLContext(context);
            OutputStream stream = connection.getOutputStream();
            stream.write(generateQueryString(payload).getBytes());
            response = new JuspayHTTPResponse(connection);
            Log.d(LOG_TAG, "Customer: " + response.getResponsePayload());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static JuspayHTTPResponse[] generateOrder(String merchantId, String orderId, String orderAmount, String customerId, String mobile, String email, Context context) {
        try {
            String[] orderUrls = new String[]{context.getString(R.string.base_url) + "/order/create"};
            String[] auth = new String[]{Base64.encodeToString((API_KEY + ":").getBytes(), Base64.DEFAULT)};
            JuspayHTTPResponse[] response = new JuspayHTTPResponse[orderUrls.length];

            for (int i = 0; i < orderUrls.length; i++) {
                String orderUrl = orderUrls[i];
                HttpsURLConnection connection = (HttpsURLConnection) (new URL(orderUrl).openConnection());

                connection.setSSLSocketFactory(new TLSSocketFactory());
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Authorization", "Basic " + auth[i]);
                connection.setRequestProperty("version", "2018-07-01");
                connection.setDoOutput(true);

                Map<String, String> payload = new HashMap<>();
                payload.put(PaymentConstants.ORDER_ID, orderId);
                payload.put(PaymentConstants.AMOUNT, orderAmount);
                payload.put("customer_id", customerId);
                payload.put("customer_email", email);
                payload.put("customer_phone", mobile);
                payload.put("return_url", context.getString(R.string.end_url));
                payload.put(PaymentConstants.DESCRIPTION, "Test Transaction");
                payload.put("options.get_client_auth_token", "true");
                payload.put("metadata.PAYU:gateway_reference_id", "BUS");
//                payload.put("options.create_mandate", "OPTIONAL");
//                payload.put("mandate_max_amount", "1000");
//                payload.put("metadata.AXIS_UPI:gateway_reference_id", "1c172215b3004f51a3af982cf1b6c1fc");

                OutputStream stream = connection.getOutputStream();
                stream.write(generateQueryString(payload).getBytes());
                response[i] = new JuspayHTTPResponse(connection);
                Log.d(LOG_TAG, "Order: " + response[i].getResponsePayload());
            }
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String generateQueryString(Map<String, String> queryString) throws UnsupportedEncodingException {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : queryString.entrySet()) {
            if (sb.length() > 0) {
                sb.append('&');
            }
            String key = entry.getKey();
            String val = entry.getValue();
            sb.append(URLEncoder.encode(key, "UTF-8")).append("=").append(URLEncoder.encode(val, "UTF-8"));
        }
        return sb.toString();
    }

    private static void initializeSSLContext(Context mContext) {
        try {
            SSLContext.getInstance("TLSv1.2");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            ProviderInstaller.installIfNeeded(mContext.getApplicationContext());
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }
}

