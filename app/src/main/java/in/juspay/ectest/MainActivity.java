package in.juspay.ectest;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

import in.juspay.godel.PaymentActivity;
import in.juspay.godel.core.PaymentConstants;
import in.juspay.godel.ui.JuspayPaymentsCallback;
import in.juspay.godel.ui.PaymentFragment;
import in.juspay.hypersdk.core.JuspayCallback;
import in.juspay.hypersdk.core.MerchantViewType;
import in.juspay.hypersdk.data.JuspayResponseHandler;
import in.juspay.hypersdk.ui.JuspayWebView;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "WrapperInteg";

    private final static int REQUEST_CODE = 88;

    PaymentFragment fragment;
    private String customerId = "Harsh123";
    private String mobile = "9876543210";
    private String email = "test@juspay.in";
    private String merchantId = "ixigo";
    private String clientId = merchantId + "_android";
    private String orderId = "";
    private String amount = "1.0";
    private Bundle bundle = new Bundle();
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        WebView.setWebContentsDebuggingEnabled(true);

        PaymentActivity.preFetch(this, clientId);

        fragment = new PaymentFragment();
        if (savedInstanceState != null) {
            fragment = (PaymentFragment) getSupportFragmentManager().getFragment(savedInstanceState, "CURRENT_FRAGMENT");
        }
    }

    @Override
    public void onBackPressed() {
        if (fragment != null && fragment.isAdded()) {
            fragment.backPressHandler(true);
        } else {
            super.onBackPressed();
        }
    }

    public void createCustomer(View view) {
        showPD();
        new Thread(() -> {
            API.createCustomer(merchantId, customerId, mobile, email, MainActivity.this);
            runOnUiThread(() -> pd.hide());
        }).start();
    }

    public void createOrder(View view) {
        showPD();
        orderId = "hyperOrder-"  + UUID.randomUUID().toString().substring(0,7);
        new Thread(() -> {
            JuspayHTTPResponse[] responses = API.generateOrder(merchantId, orderId, amount, customerId, mobile, email, this);
            try {
                if (responses != null) {
                    bundle = prepareBundle(responses);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            runOnUiThread(() -> pd.hide());
        }).start();
    }

    private Bundle prepareBundle(String orderId, JuspayHTTPResponse response) throws JSONException {
        ArrayList<String> endUrls = new ArrayList<>(Arrays.asList("https://www.reload.in/recharge/", ".*www.reload.in/payment/f.*", ".*sandbox.juspay.in\\/thankyou.*", ".*wallet.juspay.dev:8080/recharge/payment.*", ".*www.foodstag.in\\/payment-gateway\\/handle-payment.*", ".*sandbox.juspay.in\\/end.*", ".*voonik.org\\/checkout\\/juspay_callback", ".*localhost.*", ".*api.juspay.in\\/end.*"));

        Bundle ecBundle = new Bundle();
        ecBundle.putString(PaymentConstants.ORDER_ID, orderId);
        ecBundle.putString(PaymentConstants.AMOUNT, amount);
        ecBundle.putString(PaymentConstants.SERVICE, "in.juspay.ec");
        ecBundle.putStringArrayList(PaymentConstants.END_URLS, endUrls);
        ecBundle.putString(PaymentConstants.MERCHANT_ID, merchantId);
        ecBundle.putString(PaymentConstants.CLIENT_ID, clientId);
        ecBundle.putString(PaymentConstants.CUSTOMER_ID, customerId);
        ecBundle.putString(PaymentConstants.CUSTOMER_MOBILE, mobile);
        ecBundle.putString(PaymentConstants.ENV, PaymentConstants.ENVIRONMENT.SANDBOX);
        ecBundle.putBoolean(PaymentConstants.MERCHANT_LOADER, true);

        JSONObject orderDetails = new JSONObject(response.getResponsePayload());
        JSONObject auth = orderDetails.optJSONObject("juspay");
        if (auth == null) {
            return null;
        }
        ecBundle.putString(PaymentConstants.CLIENT_AUTH_TOKEN, auth.optString("client_auth_token", ""));

        return ecBundle;
    }

    private Bundle prepareBundle(JuspayHTTPResponse[] response) throws JSONException {
        JSONObject order = new JSONObject(response[0].getResponsePayload());
        return prepareBundle(order.getString("order_id"), response[0]);
    }

    public void nbTxn(View view) {
        JSONObject jsonObject = new JSONObject();
        EditText bankCode = findViewById(R.id.bankCode);
        try {
            jsonObject.put("opName", "nbTxn");
            jsonObject.put("paymentMethodType", "NB");
            jsonObject.put("paymentMethod", bankCode.getText().toString());
            jsonObject.put("redirectAfterPayment", true);
            jsonObject.put("format", "json");

            Bundle newBundle = new Bundle(bundle);
            newBundle.putString("payload", jsonObject.toString());

            startEC(newBundle);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void checkEligibility(View view) {
        EditText cardBinET = findViewById(R.id.cardBin);
        String cardBin = cardBinET.getText().toString();
        JSONObject payload = new JSONObject();

        try {
            payload.put("opName", "eligibility");

            JSONObject eligibilityData = new JSONObject();
            JSONArray cardsArray = new JSONArray();

            JSONObject card = new JSONObject();
            card.put("cardBin", cardBin);
            card.put("checkType", new JSONArray().put("otp"));

            cardsArray.put(card);
            eligibilityData.put("cards", cardsArray);
            payload.put("data", eligibilityData);

            Bundle newBundle = new Bundle(bundle);
            newBundle.putString("payload", payload.toString());

            startEC(newBundle);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void getPaymentMethods(View view) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("opName", "getPaymentMethods");

            Bundle newBundle = new Bundle(bundle);
            newBundle.putString("payload", jsonObject.toString());

            startEC(newBundle);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void cardTxnClick(View view) {

        EditText cardType = findViewById(R.id.cardType);
        EditText cardToken = findViewById(R.id.cardToken);
        EditText cardNumber = findViewById(R.id.cardNumber);
        EditText expMonth = findViewById(R.id.expMonth);
        EditText expYear = findViewById(R.id.expYear);
        EditText cvv = findViewById(R.id.cvv);
        EditText nameOnCard = findViewById(R.id.nameOnCard);
        CheckBox saveCard = findViewById(R.id.saveForFuture);
        EditText authType = findViewById(R.id.authType);

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("opName", "cardTxn");
            jsonObject.put("paymentMethodType", "CARD");
            jsonObject.put("paymentMethod", cardType.getText().toString());
            jsonObject.put("cardToken", validateString(cardToken.getText().toString()));
            jsonObject.put("cardNumber", validateString(cardNumber.getText().toString()));
            jsonObject.put("cardExpMonth", validateString(expMonth.getText().toString()));
            jsonObject.put("cardExpYear", validateString(expYear.getText().toString()));
            jsonObject.put("nameOnCard", validateString(nameOnCard.getText().toString()));
            jsonObject.put("cardSecurityCode", cvv.getText().toString());
            jsonObject.put("saveToLocker", saveCard.isChecked());
            jsonObject.put("authType", validateString(authType.getText().toString()));
            jsonObject.put("redirectAfterPayment", true);
            jsonObject.put("format", "json");
            Bundle newBundle = new Bundle(bundle);
            newBundle.putString("payload", jsonObject.toString());
            startEC(newBundle);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createWalletClick(View view) {

        EditText walletForCW = findViewById(R.id.walletForCW);

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("opName", "createWallet");
            jsonObject.put("walletName", walletForCW.getText().toString());
//            jsonObject.put("gatewayReferenceId", ""); // if applicable
            jsonObject.put("mobileNumber", "9634393464");
            Bundle newBundle = new Bundle(bundle);
            newBundle.putString("payload", jsonObject.toString());
            startEC(newBundle);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void linkWalletClick(View view) {

        EditText walletForLW = findViewById(R.id.walletForLW);
        EditText walletId = findViewById(R.id.walletId);
        EditText otp = findViewById(R.id.otp);

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("opName", "linkWallet");
            jsonObject.put("walletName", walletForLW.getText().toString());
            jsonObject.put("walletId", walletId.getText().toString());
            jsonObject.put("otp", otp.getText().toString());
//            jsonObject.put("gatewayReferenceId", ""); // if applicable
            Bundle newBundle = new Bundle(bundle);
            newBundle.putString("payload", jsonObject.toString());
            startEC(newBundle);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void walletTxnClick(View view) {

        EditText walletForTxn = findViewById(R.id.walletForTxn);
        EditText walletDirToken = findViewById(R.id.walletDirToken);
        EditText sdkPresent = findViewById(R.id.sdkPresent);
        CheckBox shouldLinkPaypal = findViewById(R.id.shouldLink);
        EditText walletEmail = findViewById(R.id.walletEmail);

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("opName", "walletTxn");
            jsonObject.put("paymentMethodType", "Wallet");
            jsonObject.put("paymentMethod", walletForTxn.getText().toString());
            if (!walletDirToken.getText().toString().equals("")) {
                jsonObject.put("directWalletToken", walletDirToken.getText().toString());
            }
            jsonObject.put("sdkPresent", sdkPresent.getText().toString());
            jsonObject.put("shouldLink", shouldLinkPaypal.isChecked());

            String mobileNumber = ((EditText) findViewById(R.id.gpay_mobile_number)).getEditableText().toString();
            if (!mobileNumber.equals("")) {
                if (!mobileNumber.contains("+91") || !mobileNumber.contains("0091")) {
                    mobileNumber = "+91" + mobileNumber;
                }
                jsonObject.put("walletMobileNumber", mobileNumber);
                jsonObject.put("walletEmailAddress", validateString(walletEmail.getText().toString()));
            }

            jsonObject.put("redirectAfterPayment", true);
            jsonObject.put("format", "json");
            Bundle newBundle = new Bundle(bundle);
            newBundle.putString("payload", jsonObject.toString());
            startEC(newBundle);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // ----- Integration Code ----- //

    public String validateString(String str) {
        return !str.equals("") ? str : null;
    }

    private void createPD() {
        pd = new ProgressDialog(this);
        pd.setMessage("Processing...");
    }

    private void showResponse(String payload) {
        Log.i(TAG, "Response: " + payload);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(payload);
        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    private void showPD() {
        if (pd == null) {
            createPD();
        }
        pd.show();
    }


    public void startEC(Bundle bundle) {
        RadioGroup integrationVariant = findViewById(R.id.integration_variant);

        if (integrationVariant.getCheckedRadioButtonId() == R.id.fragment_based) {
            // Fragment-Based Integration
            startECFragment(bundle);
        } else {
            // Activity-Based Integration
            startECActivity(bundle);
        }
    }

    public void startECActivity(Bundle bundle) {
        if (pd == null) {
            createPD();
        }
        pd.show();
        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.wtf(TAG, "onResult() called with: requestCode = [" + requestCode + "], resultCode = [" + resultCode + "], data = [" + data.getExtras().toString() + "]");
        if (requestCode == REQUEST_CODE) {
            if (pd != null)
                pd.hide();
            String response = data.getStringExtra("payload");
            if (response != null) {
                showResponse(response);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void startECFragment(Bundle bundle) {
        if (pd == null) {
            createPD();
        }
        pd.show();

        // Alternative-1
        JuspayCallback juspayCallback = new JuspayCallback() {
            @Override
            public void onResult(int requestCode, int resultCode, @NonNull Intent data) {
                Log.i(TAG, "onResult() called with: requestCode = [" + requestCode + "], resultCode = [" + resultCode + "], data = [" + data.getExtras().toString() + "]");
                if (pd != null) {
                    pd.hide();
                }
                if (requestCode == REQUEST_CODE) {
                    String response = data.getStringExtra("payload");
                    if (response != null) {
                        showResponse(response);
                    }
                    removeFragment(fragment);
                }
            }
        };

        // Alternative-2
        JuspayPaymentsCallback juspayPaymentsCallback = new JuspayPaymentsCallback() {
            @Override
            public void onStartWaitingDialogCreated(@Nullable View view) {
                Log.i(TAG, "inside onStartWaitingDialogCreated");
            }

            @Override
            public void onWebViewReady(JuspayWebView juspayWebView) {
                Log.i(TAG, "inside onWebViewReady");
            }

            @Override
            public void onEvent(String jsonEventObject, JuspayResponseHandler juspayResponseHandler) {
                Log.wtf(TAG, "onEvent() called with: jsonEventObject = [" + jsonEventObject + "]");
                try {
                    JSONObject jsonObject = new JSONObject(jsonEventObject);
                    String event = jsonObject.optString("event");

                    switch (event) {
                        case PaymentConstants.HyperEvents.SHOW_LOADER:
                            if (pd == null)
                                createPD();
                            pd.show();
                            break;
                        case PaymentConstants.HyperEvents.HIDE_LOADER:
                            if (pd != null)
                                pd.hide();
                            break;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Nullable
            @Override
            public View getMerchantView(ViewGroup viewGroup, MerchantViewType merchantViewType) {
                return null;
            }

            @Override
            public void onResult(int requestCode, int resultCode, @NonNull Intent data) {
                Log.wtf(TAG, "onResult() called with: requestCode = [" + requestCode + "], resultCode = [" + resultCode + "], data = [" + data.getExtras().toString() + "]");
                String response = data.getStringExtra("payload");
                if (response != null) {
                    showResponse(response);
                }
                removeFragment(fragment);

            }
        };

        Log.wtf(TAG, "startEC() called with: bundle = [" + bundle.toString() + "]");

        bundle.putInt("requestCode", REQUEST_CODE);
        fragment.setArguments(bundle);
        fragment.setJuspayCallback(juspayPaymentsCallback);
        showFragment(fragment);
    }

    private void showFragment(Fragment fragment) {
        if (pd != null) {
            pd.hide();
        }

        getSupportFragmentManager().beginTransaction()
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(android.R.id.content, fragment)
                .commit();
    }

    public void removeFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .remove(fragment)
                .commitAllowingStateLoss();
    }
}